(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.box1 = function() {
	this.initialize(img.box1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,156,139);


(lib.box2 = function() {
	this.initialize(img.box2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,202,110);


(lib.box3 = function() {
	this.initialize(img.box3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,192,154);


(lib.box4 = function() {
	this.initialize(img.box4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,152,183);


(lib.box5 = function() {
	this.initialize(img.box5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,163,74);


(lib.logo = function() {
	this.initialize(img.logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,147,83);


(lib.mealbox = function() {
	this.initialize(img.mealbox);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,444,284);


(lib.puppet = function() {
	this.initialize(img.puppet);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,117,220);


(lib.strip1 = function() {
	this.initialize(img.strip1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,99,140);


(lib.strip2 = function() {
	this.initialize(img.strip2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,133,186);


(lib.strip3 = function() {
	this.initialize(img.strip3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,28,60);


(lib.strip4 = function() {
	this.initialize(img.strip4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,311,310);


(lib.strip5 = function() {
	this.initialize(img.strip5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,108,94);


(lib.strip6 = function() {
	this.initialize(img.strip6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,75,170);


(lib.strip7 = function() {
	this.initialize(img.strip7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,75,129);


(lib.strip8 = function() {
	this.initialize(img.strip8);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,144,82);


(lib.strip9 = function() {
	this.initialize(img.strip9);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,64,171);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.v4MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ah2AAQABi3B1AAQB2AAAAC3QAAC4h2ABQh1gBgBi4gAgThPQgGAWAAA5QAAA6AGAWQAFAWAOAAQAPAAAFgWQAHgXgBg5QABg4gHgXQgFgWgPAAQgOAAgFAWg");
	this.shape.setTransform(100.95,37.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ah1AAQAAi3B1AAQB2AAAAC3QAAC4h2ABQh1gBAAi4gAgThPQgGAWAAA5QAAA6AGAWQAFAWAOAAQAPAAAFgWQAHgXAAg5QAAg4gHgXQgFgWgPAAQgOAAgFAWg");
	this.shape_1.setTransform(75.55,37.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgpArQApgBAAgcIAAgEIgKACQgSAAgLgMQgMgLAAgTQAAgWAOgNQANgOAVAAQAYAAAQASQAPASAAAdQAAArgaAZQgaAagpAAg");
	this.shape_2.setTransform(56.775,53.15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhNCcQgcgagCgsIBRgYQABArAXAAQAXAAgBgxQABgtgWAAQgOAAgFATIhGgDIAIjQICvAAIAABOIhmAAIgCA9QAOgOAXAAQAmAAAXAeQAVAdAAA0QAAA6gfAjQgfAigwAAQguAAgdgag");
	this.shape_3.setTransform(39.1,37.275);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhgA4IgXAAIAAgwIAUAAIAAgIIAAgLIgUAAIAAgvIAYAAQARh+BfABQAvAAAaAeQAbAdADA4IhOAUQAAg6gZgBQgXAAgFAxIAkAAIgDAvIgkAAIAAALIAAAIIAiAAIgEAwIgcAAQAGA0AXgBQAbABgBg9IBNAVQgCA3gcAfQgaAfguAAQhhgBgRiAg");
	this.shape_4.setTransform(15.075,37.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.v4MC, new cjs.Rectangle(0,0,115.9,71.3), null);


(lib.v3MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhMCcQgdgagCgsIBRgYQABArAXAAQAWAAABgxQgBgtgVAAQgOAAgFATIhGgDIAHjQICwAAIAABOIhmAAIgDA9QAOgOAYAAQAmAAAWAeQAWAdAAA0QAAA6gfAjQgeAigyAAQgtAAgcgag");
	this.shape.setTransform(90.8,37.275);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhMCkQgZgVgIglIBEgfQAEAhAYAAQASAAALgXQAKgYAAgpIAAgCIAAAAQgNAlgmAAQgnAAgYgfQgYghAAgxQAAg4AegiQAegjAvAAQA9AAAeA3QAbAvAABJQAABbgiAzQghAyg7ABQgmgBgZgUgAgbg+QAAAvAXgBQAXABAAgvQAAgtgXgBQgXABAAAtg");
	this.shape_1.setTransform(67.675,37.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgpArQApgBAAgcIAAgEIgKACQgSAAgLgMQgMgLAAgTQAAgWAOgNQANgOAVAAQAYAAAQASQAPASAAAdQAAArgaAZQgaAagpAAg");
	this.shape_2.setTransform(49.525,53.15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ah1AAQAAi3B1AAQB2AAAAC3QAAC4h2ABQh1gBAAi4gAgThPQgGAWAAA5QAAA6AGAWQAFAWAOAAQAPAAAFgWQAHgXgBg5QABg4gHgXQgFgWgPAAQgOAAgFAWg");
	this.shape_3.setTransform(30.65,37.05);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgZC2IAAkLIgqAGIAAhSICHgUIAAFrg");
	this.shape_4.setTransform(9.325,36.925);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.v3MC, new cjs.Rectangle(0,0,104.5,71.3), null);


(lib.v2MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAaC0QgIgOgCgZIgGhAQgEgagSAAIgRAAIAACBIhcAAIAAlnIB5AAQByAAAABqQAAAggQAYQgPAWgaAHIAAABQAmAKAFAsIAJBJQADAXAKARgAgdgTIATAAQAPAAAHgJQAGgKAAgYQAAgYgGgJQgHgJgPAAIgTAAg");
	this.shape.setTransform(95.65,37.075);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhcCJQgggvgBhaQABhYAggwQAhgvA7AAQA+AAAfAvQAhAwgBBYQABBaghAvQgfAwg+AAQg8AAgggwgAgVhSQgIAYABA6QgBA7AIAYQAGAZAPgBQARAAAGgYQAHgXgBg8QABg7gHgXQgGgZgRAAQgPABgGAYg");
	this.shape_1.setTransform(68.45,37.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhcCJQghgvAAhaQAAhYAhgwQAhgvA7AAQA9AAAhAvQAfAwAABYQAABagfAvQghAwg9AAQg8AAgggwgAgVhSQgIAYABA6QgBA7AIAYQAGAZAPgBQARAAAGgYQAHgXAAg8QAAg7gHgXQgGgZgRAAQgPABgGAYg");
	this.shape_2.setTransform(41.65,37.05);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgvC0IhSlnIBhAAIAhDWIABAAIAijWIBeAAIhQFng");
	this.shape_3.setTransform(15.125,37.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.v2MC, new cjs.Rectangle(0,0,110.3,71.3), null);


(lib.v1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAlC0IhCitIgCAAIAACtIhVAAIAAlnIBVAAIA+CpIACAAIAAipIBUAAIAAFng");
	this.shape.setTransform(67.625,37.075);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAnC0IgLhHIg6AAIgLBHIhXAAIBHlnIBzAAIBHFngAASAoIgSh1IgBAAIgSB1IAlAAg");
	this.shape_1.setTransform(41.275,37.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgvC0IhSlnIBhAAIAhDWIABAAIAijWIBeAAIhQFng");
	this.shape_2.setTransform(15.125,37.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.v1MC, new cjs.Rectangle(0,0,83.2,71.3), null);


(lib.toplinemc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4002B").s().p("AJYBLIAAiVIGQAAIAACVgAjHBLIAAiVIGPAAIAACVgAvnBLIAAiVIGQAAIAACVg");
	this.shape.setTransform(100,7.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.toplinemc, new cjs.Rectangle(0,0,200,15), null);


(lib.t3mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4002B").s().p("AhbCIQgdgwAAhTQAAhZAigyQAigxA8AAQAxgBAdAfQAcAfAEA2IhXAVQgDg4gWAAQgeAAAABsQAABlAfAAQAaAAAAgrIAAgLIgYAAIAAhDIBwAAIAADDIg9AAIgJgiQgSAngtAAQgyAAgdgxg");
	this.shape.setTransform(156.275,37.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4002B").s().p("AAnC0IgLhHIg6AAIgLBHIhXAAIBHlnIBzAAIBHFngAASAoIgSh1IgBAAIgSB1IAlAAg");
	this.shape_1.setTransform(130.325,37.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E4002B").s().p("Ah3C0IAAlnIBpAAQBCAAAiAuQAjAugBBXQAAC0iGAAgAgbBoIAMAAQAUAAAKgXQAIgYABg5QgBg4gIgXQgJgYgVAAIgMAAg");
	this.shape_2.setTransform(104.45,37.075);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E4002B").s().p("AhPCdQgegdgCgwIBOgVQAAAXAJAPQAIAPAPAAQAWAAAAgYQAAgLgKgLQgIgKgdgVQgxgggQgYQgPgZAAggQAAguAegeQAggdAxAAQAwAAAbAaQAcAbACAwIhOARQgBgugaAAQgUAAAAAXQAAALAHAJQAIAKAcATQArAbAQAVQAZAeAAApQAAAvgfAdQggAdgxAAQgwAAgfgcg");
	this.shape_3.setTransform(79.075,37.05);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E4002B").s().p("AAlC0IhCitIgCAAIAACtIhVAAIAAlnIBVAAIA+CpIACAAIAAipIBUAAIAAFng");
	this.shape_4.setTransform(54.375,37.075);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E4002B").s().p("AguC0IAAlnIBdAAIAAFng");
	this.shape_5.setTransform(34.775,37.075);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4002B").s().p("Ah4C0IAAlnIBqAAQBCAAAiAuQAjAuAABXQgBC0iGAAgAgcBoIANAAQAUAAAKgXQAJgYgBg5QABg4gJgXQgKgYgTAAIgOAAg");
	this.shape_6.setTransform(15.65,37.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.t3mc, new cjs.Rectangle(0,0,172,71.3), null);


(lib.t2mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4002B").s().p("AhxC0IAAlnIBwAAQBzAAAAB5QAAB5hzAAIgUAAIAAB1gAgVgKIAPAAQAbAAAAgwQAAgvgbAAIgPAAg");
	this.shape.setTransform(41.775,37.075);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4002B").s().p("AhcCJQghgvABhaQgBhYAhgwQAggvA9AAQA9AAAgAvQAfAwABBYQgBBagfAvQggAwg9AAQg9AAgggwgAgWhSQgGAYgBA6QABA7AGAYQAHAZAQgBQAPAAAHgYQAGgXABg8QgBg7gGgXQgHgZgPAAQgQABgHAYg");
	this.shape_1.setTransform(15.4,37.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.t2mc, new cjs.Rectangle(0,0,55.8,71.3), null);


(lib.t1mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4002B").s().p("AAlC0IhCitIgCAAIAACtIhVAAIAAlnIBVAAIA+CpIACAAIAAipIBUAAIAAFng");
	this.shape.setTransform(124.975,37.075);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4002B").s().p("AhcC0IAAlnIC5AAIAABMIheAAIAABCIBQAAIAABHIhQAAIAABGIBeAAIAABMg");
	this.shape_1.setTransform(101.125,37.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E4002B").s().p("AhcC0IAAlnIC5AAIAABMIheAAIAABCIBQAAIAABHIhQAAIAABGIBeAAIAABMg");
	this.shape_2.setTransform(79.675,37.075);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E4002B").s().p("AhZC0IAAlnIBcAAIAAEbIBXAAIAABMg");
	this.shape_3.setTransform(59.025,37.075);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E4002B").s().p("AhZC0IAAlnIBcAAIAAEbIBXAAIAABMg");
	this.shape_4.setTransform(38.675,37.075);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E4002B").s().p("AAnC0IgLhHIg6AAIgLBHIhXAAIBHlnIBzAAIBHFngAASAoIgSh1IgBAAIgSB1IAlAAg");
	this.shape_5.setTransform(15.025,37.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.t1mc, new cjs.Rectangle(0,0,140.5,71.3), null);


(lib.strip9MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.strip9();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.strip9MC, new cjs.Rectangle(0,0,32,85.5), null);


(lib.strip8MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.strip8();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.strip8MC, new cjs.Rectangle(0,0,72,41), null);


(lib.strip7MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.strip7();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.504,0.5039);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.strip7MC, new cjs.Rectangle(0,0,37.8,65), null);


(lib.strip6MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.strip6();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.strip6MC, new cjs.Rectangle(0,0,37.5,85), null);


(lib.strip5MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.strip5();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.strip5MC, new cjs.Rectangle(0,0,54,47), null);


(lib.strip4MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.strip4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.strip4MC, new cjs.Rectangle(0,0,155.5,155), null);


(lib.strip3MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.strip3();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.strip3MC, new cjs.Rectangle(0,0,14,30), null);


(lib.strip2MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.strip2();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.strip2MC, new cjs.Rectangle(0,0,66.5,93), null);


(lib.strip1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.strip1();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.strip1MC, new cjs.Rectangle(0,0,49.5,70), null);


(lib.mealboxMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.mealbox();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.mealboxMC, new cjs.Rectangle(0,0,222,142), null);


(lib.legalMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgFAGQgDgCAAgEQAAgDADgCQACgDADAAQAEAAACADQADACAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape.setTransform(265.075,10.075);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgUAXQgGgJgBgNQABgOAHgJQAJgIAMAAQAJAAAHAEQAGAFADAHIgOAGQgDgJgIAAQgGAAgDAEQgEAGAAAIQAAAIAEAFQADAFAGAAQAEAAAEgCQADgDAAgEIAAgCIgLAAIAAgKIAaAAIAAAgIgLAAIgCgHQgBADgEADQgFACgFAAQgMAAgIgIg");
	this.shape_1.setTransform(260.55,7.85);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAPAeIgEgMIgVAAIgEAMIgQAAIAXg7IAQAAIAVA7gAAHAGIgHgUIAAAAIgGAUIANAAg");
	this.shape_2.setTransform(254.35,7.85);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAeIAAg7IAXAAQAOAAAHAHQAIAJAAANQAAAOgIAIQgHAIgOAAgAgLASIAIAAQAGAAAEgFQAEgFAAgIQAAgQgOAAIgIAAg");
	this.shape_3.setTransform(248.3,7.85);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgPAbQgGgEgCgHIAMgFQABADADADQAEACADABQAEgBADgBQACgCAAgDQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAgBAAIgIgEIgJgDIgFgCQgGgDAAgJQAAgIAGgGQAHgFAJAAQATAAAEAPIgOADQAAgDgDgBQgCgCgEAAQgDAAgCABQgBAAAAABQAAABgBAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABQABAAAAABQAAAAABABIAIACQALADADADQAGADABAJQgBAJgGAFQgHAFgLAAQgIAAgHgEg");
	this.shape_4.setTransform(242.35,7.85);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AANAeIgYgkIAAAkIgOAAIAAg7IAOAAIAXAkIAAgkIAOAAIAAA7g");
	this.shape_5.setTransform(236.575,7.85);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgHAeIAAg7IAOAAIAAA7g");
	this.shape_6.setTransform(232,7.85);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaAeIAAg7IAXAAQAOAAAHAHQAJAJgBANQABAOgJAIQgHAIgOAAgAgKASIAHAAQAGAAAEgFQAEgFAAgIQAAgQgOAAIgHAAg");
	this.shape_7.setTransform(227.75,7.85);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgXAeIAAg7IAYAAQALAAAGAGQAGAFAAAKQAAAIgHAFQgFAGgLAAIgJAAIAAATgAgIAAIAIAAQAIAAAAgIQAAgJgIAAIgIAAg");
	this.shape_8.setTransform(219.95,7.85);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgUAWQgIgIAAgOQAAgNAIgJQAHgIANAAQANAAAIAIQAIAJAAANQAAAOgIAIQgIAJgNAAQgNAAgHgJgAgIgNQgEAFAAAIQAAAJAEAEQADAGAFAAQAGAAAEgGQADgEAAgJQAAgIgDgFQgEgEgGAAQgFAAgDAEg");
	this.shape_9.setTransform(213.85,7.85);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AANAeIgYgkIAAAkIgOAAIAAg7IAOAAIAXAkIAAgkIAOAAIAAA7g");
	this.shape_10.setTransform(205.475,7.85);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_11.setTransform(199.725,7.85);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_12.setTransform(194.475,7.85);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IAPAAIAAAvIAaAAIAAAMg");
	this.shape_13.setTransform(189.375,7.85);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IAPAAIAAAvIAaAAIAAAMg");
	this.shape_14.setTransform(184.325,7.85);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAPAeIgEgMIgVAAIgFAMIgPAAIAXg7IAQAAIAVA7gAAHAGIgHgUIAAAAIgHAUIAOAAg");
	this.shape_15.setTransform(178.5,7.85);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AANAeIgYgkIAAAkIgOAAIAAg7IAOAAIAXAkIAAgkIAOAAIAAA7g");
	this.shape_16.setTransform(170.125,7.85);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_17.setTransform(164.375,7.85);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgFAGQgDgCAAgEQAAgDADgCQACgDADAAQAEAAACADQADACAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_18.setTransform(158.425,10.075);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgUAXQgGgJgBgNQABgOAHgJQAJgIAMAAQAJAAAHAEQAGAFADAHIgOAGQgDgJgIAAQgGAAgDAEQgEAGAAAIQAAAIAEAFQADAFAGAAQAEAAAEgCQADgDAAgEIAAgCIgLAAIAAgKIAaAAIAAAgIgLAAIgCgHQgBADgEADQgFACgFAAQgMAAgIgIg");
	this.shape_19.setTransform(153.9,7.85);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgGAeIAAg7IANAAIAAA7g");
	this.shape_20.setTransform(149.5,7.85);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgaAeIAAg7IAXAAQAOAAAHAHQAIAJAAANQAAAOgIAIQgHAIgOAAgAgLASIAIAAQAGAAAEgFQAEgFAAgIQAAgQgOAAIgIAAg");
	this.shape_21.setTransform(145.25,7.85);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IAPAAIAAAvIAaAAIAAAMg");
	this.shape_22.setTransform(139.625,7.85);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_23.setTransform(134.425,7.85);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUAXQgGgJAAgNQAAgOAIgJQAHgIANAAQAJAAAHAEQAHAFACAHIgPAGQgBgJgJAAQgGAAgDAEQgEAGAAAIQAAAIAEAFQADAFAGAAQAFAAADgCQACgDAAgEIAAgCIgKAAIAAgKIAaAAIAAAgIgKAAIgDgHQgBADgEADQgFACgFAAQgMAAgIgIg");
	this.shape_24.setTransform(128.45,7.85);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgUAXQgGgJgBgNQABgOAHgJQAJgIAMAAQAJAAAHAEQAGAFADAHIgOAGQgDgJgIAAQgGAAgDAEQgEAGAAAIQAAAIAEAFQADAFAGAAQAEAAAEgCQADgDAAgEIAAgCIgLAAIAAgKIAaAAIAAAgIgLAAIgCgHQgBADgEADQgFACgFAAQgMAAgIgIg");
	this.shape_25.setTransform(120.15,7.85);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgGAeIAAg7IANAAIAAA7g");
	this.shape_26.setTransform(115.75,7.85);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAJAeIgJgmIAAAAIgIAmIgPAAIgPg7IAPAAIAJAoIAAAAIAJgoIAMAAIAJAoIAAAAIAJgoIAOAAIgOA7g");
	this.shape_27.setTransform(110.375,7.85);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgSAYQgGgGAAgMIAAgjIAPAAIAAAiQAAANAJAAQAKAAAAgNIAAgiIAPAAIAAAjQAAAMgGAGQgHAGgMABQgLgBgHgGg");
	this.shape_28.setTransform(103.325,7.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_29.setTransform(97.775,7.85);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_30.setTransform(92.525,7.85);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgHAeIAAguIgTAAIAAgNIA0AAIAAANIgTAAIAAAug");
	this.shape_31.setTransform(84.95,7.85);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_32.setTransform(79.525,7.85);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgHAeIAAg7IAPAAIAAA7g");
	this.shape_33.setTransform(75.45,7.85);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AANAeIgYgkIAAAkIgOAAIAAg7IAOAAIAXAkIAAgkIAOAAIAAA7g");
	this.shape_34.setTransform(70.875,7.85);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgPAbQgHgEgCgHIAOgFQAAADADADQAEACADABQAEgBADgBQACgCABgDQgBAAAAgBQAAAAAAgBQAAAAgBgBQAAAAgBAAIgIgEIgJgDIgFgCQgHgDAAgJQAAgIAHgGQAHgFAJAAQAUAAADAPIgOADQAAgDgCgBQgDgCgEAAQgDAAgCABQAAAAgBABQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAABQAAAAABABQAAAAAAABQABAAAAABIAIACQALADADADQAHADAAAJQAAAJgHAFQgHAFgLAAQgJAAgGgEg");
	this.shape_35.setTransform(62.95,7.85);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgGAeIAAg7IANAAIAAA7g");
	this.shape_36.setTransform(59.05,7.85);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgaAeIAAg7IAXAAQAOAAAHAHQAJAJgBANQABAOgJAIQgHAIgOAAgAgKASIAHAAQAGAAAEgFQAEgFAAgIQAAgQgOAAIgHAAg");
	this.shape_37.setTransform(52.8,7.85);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgUAWQgIgIAAgOQAAgNAIgJQAHgIANAAQANAAAIAIQAIAJAAANQAAAOgIAIQgIAJgNAAQgNAAgHgJgAgIgNQgEAFAAAIQAAAJAEAEQADAGAFAAQAGAAAEgGQADgEAAgJQAAgIgDgFQgEgEgGAAQgFAAgDAEg");
	this.shape_38.setTransform(46.45,7.85);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgXAeIAAg7IAaAAQAJAAAFAEQAFAFAAAHQAAAFgDAEQgDADgFABIAAAAQAGAAAEAEQADAEAAAFQAAAIgFAFQgGAEgKAAgAgJATIAJAAQAJAAAAgHQAAgIgJAAIgJAAgAgJgEIAJAAQAHABAAgIQAAgHgHAAIgJAAg");
	this.shape_39.setTransform(40.675,7.85);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AANAeIgYgkIAAAkIgOAAIAAg7IAOAAIAXAkIAAgkIAOAAIAAA7g");
	this.shape_40.setTransform(34.425,7.85);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAPAeIgEgMIgVAAIgFAMIgOAAIAVg7IAQAAIAXA7gAAHAGIgHgUIAAAAIgHAUIAOAAg");
	this.shape_41.setTransform(28.05,7.85);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AAPAeIgEgMIgVAAIgFAMIgOAAIAVg7IAQAAIAXA7gAAHAGIgHgUIAAAAIgHAUIAOAAg");
	this.shape_42.setTransform(21.75,7.85);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgHAeIAAguIgTAAIAAgNIA0AAIAAANIgTAAIAAAug");
	this.shape_43.setTransform(13.75,7.85);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgHAeIAAg7IAPAAIAAA7g");
	this.shape_44.setTransform(9.5,7.85);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgZAeIAAg7IAWAAQANAAAJAHQAHAJABANQgBAOgHAIQgJAIgNAAgAgKASIAGAAQAHAAAEgFQAEgFAAgIQAAgQgPAAIgGAAg");
	this.shape_45.setTransform(5.25,7.85);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.legalMC, new cjs.Rectangle(0,0,268.5,15.2), null);


(lib.l2MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgzAAQAAhRAzAAQA1AAgBBRQABBSg1AAQgzAAAAhSgAgIgjQgCAKgBAZQABAZACALQACAJAGAAQAGAAADgJQADgLAAgZQAAgYgDgLQgDgKgGABQgGAAgCAJg");
	this.shape.setTransform(71,25.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgzAAQgBhRA0AAQA1AAAABRQAABSg1AAQg0AAABhSgAgIgjQgCAKAAAZQAAAZACALQADAJAFAAQAHAAACgJQADgLgBgZQABgYgDgLQgCgKgHABQgFAAgDAJg");
	this.shape_1.setTransform(59.7,25.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgmAnQAmAAAAgbIAAgCIgJABQgQAAgLgLQgLgKAAgSQAAgTANgNQAMgNATAAQAXAAAOARQAOAQAAAbQAAAngYAYQgXAYgnAAg");
	this.shape_2.setTransform(52.75,49.35);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhHCRQgbgZgBgpIBLgVQABAnAVAAQAVAAAAgtQAAgpgUAAQgMgBgGASIhAgDIAGjBICjAAIAABJIheAAIgDA4QANgNAWAAQAkAAAUAcQAUAaAAAxQAAA2gdAgQgcAggtAAQgrAAgagYg");
	this.shape_3.setTransform(36.4,34.65);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhZAzIgVAAIAAgsIATAAIAAgHIAAgLIgTAAIAAgsIAWAAQAQhzBYAAQArAAAZAcQAYAcADAzIhHASQAAg2gYAAQgVABgFArIAiAAIgEAsIghAAIAAALIAAAHIAfAAIgDAsIgaAAQAGAxAVgBQAYAAAAg3IBIASQgDA0gZAcQgZAdgqAAQhaAAgQh4g");
	this.shape_4.setTransform(14.1,34.45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.l2MC, new cjs.Rectangle(0,0,78.9,66.3), null);


(lib.l1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgiBFQgNgLAAgUIAkgKQAAATALAAQAJAAAAgWQAAgUgJAAQgGAAgDAIIgegBIADhcIBNAAIAAAjIgsAAIgCAbQAGgGAKAAQASAAAJANQAKAMAAAYQAAAZgOAQQgNAPgVAAQgVAAgNgMg");
	this.shape.setTransform(41.45,17.675);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AghBIQgMgJgDgRIAfgNQABAPALAAQAIAAAEgLQAFgKAAgSIAAgBIAAAAQgGARgRAAQgRAAgLgPQgKgNAAgXQAAgYAOgPQANgQAUAAQAbAAAOAZQALAUAAAhQAAAngOAYQgQAWgZAAQgRAAgLgKgAgLgbQAAAVAKgBQAKABAAgVQAAgUgKAAQgKAAAAAUg");
	this.shape_1.setTransform(31.15,17.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgSATQASAAAAgNIAAgCIgEABQgIAAgFgFQgFgEAAgJQAAgJAGgHQAGgFAJAAQALAAAGAHQAHAJAAANQAAARgMAMQgLAMgSgBg");
	this.shape_2.setTransform(23.15,24.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgzAAQAAhRAzAAQA1AAgBBRQABBSg1AAQgzAAAAhSgAgIgjQgCAKgBAZQABAZACAKQACAKAGAAQAGAAADgKQADgKAAgZQAAgYgDgLQgDgKgGABQgGAAgCAJg");
	this.shape_3.setTransform(14.75,17.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgKBRIAAh2IgTADIAAglIA7gJIAAChg");
	this.shape_4.setTransform(5.25,17.525);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.l1MC, new cjs.Rectangle(0,0,48.8,33.9), null);


(lib.kfclogo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.logo();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.kfclogo, new cjs.Rectangle(0,0,147,83), null);


(lib.d1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhCCGIAAkLIBFAAIAADSIBAAAIAAA5g");
	this.shape.setTransform(173.575,70.95);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAdCGIgIg1IgrAAIgIA1IhAAAIA0kLIBVAAIA0ELgAANAdIgNhWIgBAAIgNBWIAbAAg");
	this.shape_1.setTransform(156.05,70.95);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhECGIAAkLICJAAIAAA5IhGAAIAAAxIA7AAIAAA0Ig7AAIAAA0IBGAAIAAA5g");
	this.shape_2.setTransform(138.6,70.95);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhYCGIAAkLIBOAAQAwAAAaAjQAZAhAABCQAACFhjAAgAgUBNIAJAAQAPAAAGgSQAHgRAAgpQAAgqgHgSQgGgRgPAAIgJAAg");
	this.shape_3.setTransform(121.025,70.95);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ag5BzQgVgTgBggIA8gSQAAAfARAAQARABAAglQAAgggPgBQgKAAgFAOIg0gCIAGiaICBAAIAAA6IhKAAIgCAtQAJgLASABQAcgBARAXQAQAVAAAnQAAArgXAaQgXAZgkAAQgiAAgVgUg");
	this.shape_4.setTransform(95.6,71.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AATCGQgGgLgCgSIgDgxQgEgSgNgBIgMAAIAABhIhEAAIAAkLIBZAAQBUAAAABPQAAAYgLARQgMARgTAFIAAABQAcAHAEAgIAHA2QACASAIANgAgVgOIAOAAQAKABAGgIQAEgHAAgRQAAgSgEgHQgGgHgKAAIgOAAg");
	this.shape_5.setTransform(71.4,70.95);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhEBmQgYgjAAhDQAAhBAYgjQAYgkAsAAQAtAAAYAkQAYAjAABBQAABDgYAjQgXAjguAAQgsAAgYgjgAgQg8QgFASAAAqQAAAsAFASQAFASALAAQAMAAAFgSQAFgSAAgsQAAgrgFgSQgFgSgMAAQgLAAgFATg");
	this.shape_6.setTransform(51.25,70.925);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhEBmQgYgjAAhDQAAhBAYgjQAYgkAsAAQAtAAAYAkQAYAjAABBQAABDgYAjQgYAjgtAAQgsAAgYgjgAgQg8QgFASAAAqQAAAsAFASQAFASALAAQAMAAAFgSQAFgSAAgsQAAgrgFgSQgFgSgMAAQgLAAgFATg");
	this.shape_7.setTransform(31.4,70.925);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgiCGIg9kLIBIAAIAYCgIABAAIAZigIBFAAIg7ELg");
	this.shape_8.setTransform(11.7,70.95);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AhHCRQgbgZgBgpIBLgVQABAnAVAAQAUAAABgtQgBgpgTAAQgNgBgFASIhBgDIAIjBICiAAIAABJIheAAIgDA4QANgNAWAAQAjAAAVAcQAUAaAAAxQAAA2gdAgQgcAgguAAQgqAAgagYg");
	this.shape_9.setTransform(218.8,34.65);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgZBdIAAhDIhDAAIAAgyIBDAAIAAhEIAzAAIAABEIBDAAIAAAyIhDAAIAABDg");
	this.shape_10.setTransform(197.725,35.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhHCRQgagZgCgpIBLgVQABAnAVAAQAUAAAAgtQAAgpgTAAQgNgBgFASIhBgDIAIjBICiAAIAABJIhfAAIgCA4QANgNAWAAQAjAAAVAcQAUAaAAAxQAAA2gdAgQgcAgguAAQgpAAgbgYg");
	this.shape_11.setTransform(176.3,34.65);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AhUB+QgbgtAAhMQAAhTAgguQAfguA4AAQAsAAAbAcQAbAcADAzIhRATQgCg0gUABQgcgBAABlQAABdAdAAQAYAAAAgnIAAgLIgXAAIAAg9IBoAAIAAC0Ig5AAIgIggQgQAkgqAAQguAAgbgtg");
	this.shape_12.setTransform(144.975,34.45);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAkCnIgKhCIg2AAIgKBCIhQAAIBBlNIBrAAIBBFNgAAQAlIgQhtIgBAAIgRBtIAiAAg");
	this.shape_13.setTransform(120.925,34.45);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AhvCnIAAlNIBiAAQA8AAAhAqQAfAsAABQQAACnh8AAgAgZBgIALAAQATAAAIgVQAIgWAAg1QAAgzgIgWQgIgWgTAAIgLAAg");
	this.shape_14.setTransform(96.95,34.45);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AhJCRQgcgagCgsIBIgVQAAAWAIANQAJAOANAAQAUAAAAgVQAAgLgIgKQgJgKgagTQgtgdgPgXQgOgXAAgdQAAgsAcgbQAdgbAtAAQAsAAAaAZQAaAYACAtIhJAQQgBgrgYAAQgSAAAAAVQAAAKAHAJQAHAJAaARQAnAZAQAUQAXAdAAAmQAAAqgdAbQgeAbgtAAQgtAAgcgag");
	this.shape_15.setTransform(73.45,34.45);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAiCnIg9ihIgCAAIAAChIhPAAIAAlNIBPAAIA5CdIACAAIAAidIBOAAIAAFNg");
	this.shape_16.setTransform(50.55,34.45);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgqCnIAAlNIBVAAIAAFNg");
	this.shape_17.setTransform(32.375,34.45);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AhuCnIAAlNIBhAAQA9AAAfAqQAgAsABBQQAACnh9AAgAgaBgIAMAAQATAAAIgVQAJgWAAg1QAAgzgJgWQgIgWgSAAIgNAAg");
	this.shape_18.setTransform(14.65,34.45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.d1MC, new cjs.Rectangle(0,0,231.7,96.8), null);


(lib.cta = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.pina = function(){
			
			TweenMax.killTweensOf(mask);
			mask.x = -60;
			TweenMax
			.to(mask, 1.5, {
		        x: 205,
		        ease: Circ.easeOut,
				repeat:2,
		        delay: 0
		    });
			}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ApXCvIAAldISvAAIAAFdg");
	mask.setTransform(-60,17.5);

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4002B").s().p("AgXAfQgLgMAAgTQAAgTALgKQAJgLARAAQAMgBAIAGQAJAFADAKIgSAJQgDgMgLAAQgHgBgFAHQgEAGAAALQAAALAEAHQAFAGAHAAQALAAADgMIASAIQgDAKgJAGQgIAFgMABQgRgBgJgKg");
	this.shape.setTransform(103.4,17.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4002B").s().p("AgbAoIAAhPIA3AAIAAARIgjAAIAAASIAfAAIAAAPIgfAAIAAAdg");
	this.shape_1.setTransform(96.375,17.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E4002B").s().p("AAMAoIgVggIgGAAIAAAgIgUAAIAAhPIAUAAIAAAfIAHAAIATgfIAXAAIgaAnIAcAog");
	this.shape_2.setTransform(89.175,17.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E4002B").s().p("AAMAoIgMg0IAAAAIgLA0IgVAAIgThPIAUAAIAMA1IALg1IARAAIAMA1IAMg1IATAAIgTBPg");
	this.shape_3.setTransform(76.775,17.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E4002B").s().p("AgYAgQgJgIAAgQIAAgwIAUAAIAAAvQAAARANAAQANAAAAgRIAAgvIAVAAIAAAwQAAAQgJAIQgJAJgQAAQgPAAgJgJg");
	this.shape_4.setTransform(67.375,17.875);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E4002B").s().p("AgbAfQgKgMAAgTQAAgSAKgLQALgLAQAAQASAAAKALQAKALAAASQAAATgKAMQgKAKgSABQgQgBgLgKgAgMgRQgEAGAAALQAAALAEAHQAFAGAHAAQAIAAAFgGQAFgHAAgLQAAgLgFgGQgFgHgIAAQgHAAgFAHg");
	this.shape_5.setTransform(59.125,17.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4002B").s().p("AgTAiQgIgGgCgMIATgGQAAAOAKAAQAKAAAAgMIAAg0IAUAAIAAAzQAAAOgIAIQgIAIgOAAQgLAAgIgHg");
	this.shape_6.setTransform(51.275,17.875);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E4002B").s().p("AgjAoIAAhPIAfAAQASAAALAKQALAKgBATQABATgLALQgLAKgSAAgAgPAXIAJAAQAJAAAGgGQAFgGABgLQAAgWgVAAIgJAAg");
	this.shape_7.setTransform(41.55,17.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E4002B").s().p("AARAoIgggxIAAAAIAAAxIgTAAIAAhPIATAAIAfAwIAAAAIAAgwIATAAIAABPg");
	this.shape_8.setTransform(32.9,17.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E4002B").s().p("AgJAoIAAhPIATAAIAABPg");
	this.shape_9.setTransform(26.8,17.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E4002B").s().p("AgKAoIgchPIAVAAIARA3IABAAIARg3IAVAAIgcBPg");
	this.shape_10.setTransform(21.025,17.8);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("ApNClIAAlJISbAAIAAFJg");
	this.shape_11.setTransform(60.0172,17.5106,1.017,1.0606);

	var maskedShapeInstanceList = [this.shape_11];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(1));

	// Layer_5
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgXAfQgLgMAAgTQAAgTALgKQAJgLARAAQAMgBAIAGQAJAFADAKIgSAJQgDgMgLAAQgHgBgFAHQgEAGAAALQAAALAEAHQAFAGAHAAQALAAADgMIASAIQgDAKgJAGQgIAFgMABQgRgBgJgKg");
	this.shape_12.setTransform(103.4,17.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgbAoIAAhPIA3AAIAAARIgjAAIAAASIAfAAIAAAPIgfAAIAAAdg");
	this.shape_13.setTransform(96.375,17.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAMAoIgVggIgGAAIAAAgIgUAAIAAhPIAUAAIAAAfIAHAAIATgfIAXAAIgaAnIAcAog");
	this.shape_14.setTransform(89.175,17.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAMAoIgMg0IAAAAIgLA0IgVAAIgThPIAUAAIAMA1IALg1IARAAIAMA1IAMg1IATAAIgTBPg");
	this.shape_15.setTransform(76.775,17.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgYAgQgJgIAAgQIAAgwIAUAAIAAAvQAAARANAAQANAAAAgRIAAgvIAVAAIAAAwQAAAQgJAIQgJAJgQAAQgPAAgJgJg");
	this.shape_16.setTransform(67.375,17.875);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgbAfQgKgMAAgTQAAgSAKgLQALgLAQAAQASAAAKALQAKALAAASQAAATgKAMQgKAKgSABQgQgBgLgKgAgMgRQgEAGAAALQAAALAEAHQAFAGAHAAQAIAAAFgGQAFgHAAgLQAAgLgFgGQgFgHgIAAQgHAAgFAHg");
	this.shape_17.setTransform(59.125,17.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgTAiQgIgGgCgMIATgGQAAAOAKAAQAKAAAAgMIAAg0IAUAAIAAAzQAAAOgIAIQgIAIgOAAQgLAAgIgHg");
	this.shape_18.setTransform(51.275,17.875);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgjAoIAAhPIAfAAQASAAALAKQALAKgBATQABATgLALQgLAKgSAAgAgPAXIAJAAQAJAAAGgGQAFgGABgLQAAgWgVAAIgJAAg");
	this.shape_19.setTransform(41.55,17.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AARAoIgggxIAAAAIAAAxIgTAAIAAhPIATAAIAfAwIAAAAIAAgwIATAAIAABPg");
	this.shape_20.setTransform(32.9,17.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgJAoIAAhPIATAAIAABPg");
	this.shape_21.setTransform(26.8,17.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgKAoIgchPIAVAAIARA3IABAAIARg3IAVAAIgcBPg");
	this.shape_22.setTransform(21.025,17.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12}]}).wait(1));

	// Layer_1
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FFFFFF").ss(2,1,1).p("ApXiuISvAAIAAFdIyvAAg");
	this.shape_23.setTransform(60,17.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta, new cjs.Rectangle(-1,-1,122,37), null);


(lib.crossline2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AjCAUIAAgnIGFAAIAAAng");
	this.shape.setTransform(25,2,1.2804,1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.crossline2, new cjs.Rectangle(0,0,50,4), null);


(lib.crossline = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AnLAoIAAhPIOXAAIAABPg");
	this.shape.setTransform(52.4999,2.9999,1.1419,0.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.crossline, new cjs.Rectangle(0,0,105,6), null);


(lib.box5MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.box5();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.box5MC, new cjs.Rectangle(0,0,81.5,37), null);


(lib.box4MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.box4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.box4MC, new cjs.Rectangle(0,0,76,91.5), null);


(lib.box3MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.box3();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.box3MC, new cjs.Rectangle(0,0,96,77), null);


(lib.box2Mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.box2();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.box2Mc, new cjs.Rectangle(0,0,101,55), null);


(lib.box1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.box1();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5035,0.5036);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.box1MC, new cjs.Rectangle(0,0,78.6,70), null);


(lib.redBG = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.crossline = new lib.crossline();
	this.crossline.name = "crossline";
	this.crossline.parent = this;
	this.crossline.setTransform(95.1,358.8,1,1.0021,0,0,0,0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.crossline).wait(1));

	// Layer_3
	this.v4MC = new lib.v4MC();
	this.v4MC.name = "v4MC";
	this.v4MC.parent = this;
	this.v4MC.setTransform(389.15,406.85,1,1,0,0,0,55.9,35.6);

	this.v2MC = new lib.v3MC();
	this.v2MC.name = "v2MC";
	this.v2MC.parent = this;
	this.v2MC.setTransform(375.1,360.85,1,1,0,0,0,43.9,35.6);

	this.v3MC = new lib.v2MC();
	this.v3MC.name = "v3MC";
	this.v3MC.parent = this;
	this.v3MC.setTransform(-16.5,406.85,1,1,0,0,0,113.5,35.6);

	this.v1MC = new lib.v1MC();
	this.v1MC.name = "v1MC";
	this.v1MC.parent = this;
	this.v1MC.setTransform(-12,360.85,1,1,0,0,0,88,35.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.v1MC},{t:this.v3MC},{t:this.v2MC},{t:this.v4MC}]}).wait(1));

	// Layer_2
	this.instance = new lib.puppet();
	this.instance.parent = this;
	this.instance.setTransform(42,451,0.5,0.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EAEsBFnMAAAhGTIDIAAMAAABGTgEgBjBFnMAAAhGTIDHAAMAAABGTgEgHzBFnMAAAhGTIDIAAMAAABGTgAEs+hMAAAgnFIDIAAMAAAAnFgAhj+hMAAAgnFIDHAAMAAAAnFgAnz+hMAAAgnFIDIAAMAAAAnFg");
	this.shape.setTransform(70,445.45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4002B").s().p("EgY/BV8MAAAir3MAx/AAAMAAACr3g");
	this.shape_1.setTransform(160,550);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.redBG, new cjs.Rectangle(-130,0,579.1,1100), null);


// stage content:
(lib.kfc_320x240 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var d = 0.5;
		var self = this;
		var stageHeight = 240;
		var stageWidth = 320;
		var offsetX = 57;
		
		TweenMax.delayedCall(d, animateInFrame1);
		
		
		function animateInFrame1() {
		
		
		
		    var d = 0;
		    var wingToY = [16, -30, 23, -20, 109, 95, 182, 218, 187];
		    var wingToYBack = -stageHeight;
		
		    self.t1MC.y = self.t2MC.y = self.t3MC.y = self.toplineMC.y = -stageHeight;
		    self.redBG.y = stageHeight * 2;
		    self.redBG.crossline.scaleX = 0;
		    self.logoMC.scaleX = self.logoMC.scaleY = 0.5;
		    self.logoMC.x = 230;
		
		
		
		    // anim start
		
		    TweenMax.to(self.toplineMC, 0.75, {
		        y: 7,
		        ease: Expo.easeOut,
		        delay: d
		    });
		
		    //F1 text
		    TweenMax.to(self.t3MC, 0.85, {
		        y: 140,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		    d += 0.1;
		    TweenMax.to(self.t2MC, 0.85, {
		        y: 98,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		    d += 0.1;
		    TweenMax.to(self.t1MC, 0.85, {
		        y: 98,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		
		    //F1 Wings
		    for (var i = 0; i < 9; i++) {
		
		        TweenMax.to(self["strip" + i + "MC"], 3, {
		            y: wingToY[i],
		            ease: Elastic.easeOut.config(0.6, 0.6),
		            delay: 0.02 * i
		        });
		        TweenMax.to(self["strip" + i + "MC"], 3, {
		            y: wingToYBack,
		            ease: Elastic.easeInOut.config(0.5, 0.4),
		            delay: 1.2 + 0.01 * i
		        });
		
		    }
		
		    //F1 text out
		    TweenMax.to(self.toplineMC, 0.75, {
		        y: -50,
		        ease: Expo.easeInOut,
		        delay: 2.5
		    });
		
		    TweenMax.to([self.t3MC, self.t2MC, self.t1MC], 1.5, {
		        y: -stageHeight,
		        ease: Expo.easeInOut,
		        delay: 2
		    });
		    d -= 0.2;
			TweenMax.delayedCall(d, animateInFrame2);
		}
		
		function animateInFrame2() {
		
		
		    var d = 0;
		
		    d += 1.75
		    TweenMax.to(self.redBG, 2, {
		        y: -200,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    // F2 text in 
		    d += 0.9;
		    TweenMax.to(self.redBG.v3MC, 0.85, {
		        x: 130,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		    d += 0.1;
		    TweenMax.to(self.redBG.v4MC, 0.85, {
		        x: 190,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		    d += 0.05;
		    TweenMax.to(self.redBG.v2MC, 0.85, {
		        x: 150,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		    TweenMax.to(self.redBG.crossline, 0.5, {
		        x: self.redBG.crossline.x + 10,
		        scaleX: 1.0,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    d += 0.05;
		    TweenMax.to(self.redBG.v1MC, 0.85, {
		        x: 105,
		        ease: Elastic.easeOut.config(0.75, 0.4),
		        delay: d
		    });
		
		    d += 1.25;
		    TweenMax.delayedCall(d, animateInFrame3);
		}
		
		function animateInFrame3() {
		
		
		    var d = 0;
		
		    TweenMax.to(self.redBG, 2, {
		        y: -725,
		        ease: Expo.easeInOut,
		        delay: d
		    });
			 TweenMax.to(self.d1MC, 2, {
		        y: 35,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    TweenMax.to(self.logoMC, 1.0, {
		        y: 10,
		        ease: Sine.easeInOut,
		        delay: d
		    });
		
		    d += 1.11;
		   
		    TweenMax.to(self.d1MC, 0.8, {
		        scaleX: 0.8,
		        scaleY: 0.8,
		        ease: Elastic.easeOut.config(0.75, 0.4),
		        delay: d
		    });
		   
			 TweenMax.to(self.legalMC, 0.5, {
		        alpha: 1.0,
		        ease: Circ.easeOut,
		        delay: d
		    });
			d += 0.1;
			 TweenMax.to(self.mealboxMC, 1, {
		        y: 113,
		        ease: Bounce.easeOut,
		        delay: d
		    });
		    d += 1;
		    TweenMax.to(self.box1MC, 0.4, {
		        y: 95,
		       ease: Sine.easeOut,
		        delay: d
		    });
		    d += 0.1;
			TweenMax.to(self.box2MC, 0.4, {
		        y: 110,
		       ease: Sine.easeOut,
		        delay: d
		    });
			d += 0.1;
			TweenMax.to(self.box3MC, 0.4, {
		        y: 91,
		       ease: Sine.easeOut,
		        delay: d
		    });
			d += 0.1;
			TweenMax.to(self.box4MC, 0.4, {
		        y: 102,
		       ease: Sine.easeOut,
		        delay: d
		    });
			d += 0.1;
			TweenMax.to(self.box5MC, 0.4, {
		        y: 167,
		       ease: Sine.easeOut,
		        delay: d
		    });
			
		    TweenMax.delayedCall(d, animateInFrame4);
		
		
		}
		
		function animateInFrame4() {
		
		    var d = 1;
		
			
		
		    TweenMax.to(self.l1MC, 0.3, {
		        alpha: 1.0,
		        x: 70,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    TweenMax.to(self.l2MC, 0.3, {
		        alpha: 1.0,
		        x: 70,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    d += 0.2;
			TweenMax.to(self.mealboxMC, 0.3, {
		        x: self.mealboxMC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
		
		    TweenMax.to(self.box1MC, 0.3, {
		        x: self.box1MC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
		    
		   TweenMax.to(self.box2MC, 0.3, {
		        x: self.box2MC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
			
			TweenMax.to(self.box3MC, 0.3, {
		        x: self.box3MC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
			
			TweenMax.to(self.box4MC, 0.3, {
		        x: self.box4MC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
		
			TweenMax.to(self.box5MC, 0.3, {
		        x: self.box5MC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
		
		    d += 0.1;
		    TweenMax.to(self.l1MC, 0.5, {
		        x: 20,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    TweenMax.to(self.l2MC, 0.5, {
		        x: 17,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    TweenMax.to(self.crossline2, 0.7, {
		        x: 20,
		        ease: Expo.easeInOut,
		        delay: d
		    });
			d += 0.5;
			TweenMax
			.to(self.ctaMC, 0.5, {
		        alpha: 1.0,
		        ease: Circ.easeOut,
		        delay: d
		    });
			d += 0.5;
			
			
			TweenMax.delayedCall(d, self.ctaMC.pina);
			
			
		
		
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// js
	this.box5MC = new lib.box5MC();
	this.box5MC.name = "box5MC";
	this.box5MC.parent = this;
	this.box5MC.setTransform(212,-378);

	this.box2MC = new lib.box2Mc();
	this.box2MC.name = "box2MC";
	this.box2MC.parent = this;
	this.box2MC.setTransform(194.65,-435,1,1,6.2133);

	this.box4MC = new lib.box4MC();
	this.box4MC.name = "box4MC";
	this.box4MC.parent = this;
	this.box4MC.setTransform(218,-443);

	this.box3MC = new lib.box3MC();
	this.box3MC.name = "box3MC";
	this.box3MC.parent = this;
	this.box3MC.setTransform(179,-454);

	this.box1MC = new lib.box1MC();
	this.box1MC.name = "box1MC";
	this.box1MC.parent = this;
	this.box1MC.setTransform(152.45,-450);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.box1MC},{t:this.box3MC},{t:this.box4MC},{t:this.box2MC},{t:this.box5MC}]}).wait(1));

	// border
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("A4/yvMAx/AAAMAAAAlfMgx/AAAg");
	this.shape.setTransform(160,120);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// f4
	this.ctaMC = new lib.cta();
	this.ctaMC.name = "ctaMC";
	this.ctaMC.parent = this;
	this.ctaMC.setTransform(20,185);
	this.ctaMC.alpha = 0;

	this.mealboxMC = new lib.mealboxMC();
	this.mealboxMC.name = "mealboxMC";
	this.mealboxMC.parent = this;
	this.mealboxMC.setTransform(93,-432);

	this.strip8MC = new lib.strip9MC();
	this.strip8MC.name = "strip8MC";
	this.strip8MC.parent = this;
	this.strip8MC.setTransform(231,-103);

	this.strip7MC = new lib.strip8MC();
	this.strip7MC.name = "strip7MC";
	this.strip7MC.parent = this;
	this.strip7MC.setTransform(103,-72);

	this.strip6MC = new lib.strip7MC();
	this.strip6MC.name = "strip6MC";
	this.strip6MC.parent = this;
	this.strip6MC.setTransform(14,-118);

	this.strip5MC = new lib.strip6MC();
	this.strip5MC.name = "strip5MC";
	this.strip5MC.parent = this;
	this.strip5MC.setTransform(267,-195);

	this.strip4MC = new lib.strip5MC();
	this.strip4MC.name = "strip4MC";
	this.strip4MC.parent = this;
	this.strip4MC.setTransform(5,-181);

	this.strip3MC = new lib.strip4MC();
	this.strip3MC.name = "strip3MC";
	this.strip3MC.parent = this;
	this.strip3MC.setTransform(246.65,-310);

	this.strip2MC = new lib.strip3MC();
	this.strip2MC.name = "strip2MC";
	this.strip2MC.parent = this;
	this.strip2MC.setTransform(207,-237);

	this.d1MC = new lib.d1MC();
	this.d1MC.name = "d1MC";
	this.d1MC.parent = this;
	this.d1MC.setTransform(17,788.5);

	this.ctaMC_1 = new lib.cta();
	this.ctaMC_1.name = "ctaMC_1";
	this.ctaMC_1.parent = this;
	this.ctaMC_1.setTransform(20,-380);
	this.ctaMC_1.alpha = 0;

	this.crossline2 = new lib.crossline2();
	this.crossline2.name = "crossline2";
	this.crossline2.parent = this;
	this.crossline2.setTransform(-92,118.8);

	this.logoMC = new lib.kfclogo();
	this.logoMC.name = "logoMC";
	this.logoMC.parent = this;
	this.logoMC.setTransform(153,-93);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.logoMC},{t:this.crossline2},{t:this.ctaMC_1},{t:this.d1MC},{t:this.strip2MC},{t:this.strip3MC},{t:this.strip4MC},{t:this.strip5MC},{t:this.strip6MC},{t:this.strip7MC},{t:this.strip8MC},{t:this.mealboxMC},{t:this.ctaMC}]}).wait(1));

	// f3
	this.l2MC = new lib.l2MC();
	this.l2MC.name = "l2MC";
	this.l2MC.parent = this;
	this.l2MC.setTransform(-71.45,118.8);
	this.l2MC.alpha = 0;

	this.l1MC = new lib.l1MC();
	this.l1MC.name = "l1MC";
	this.l1MC.parent = this;
	this.l1MC.setTransform(-92,102.8);

	this.legalMC = new lib.legalMC();
	this.legalMC.name = "legalMC";
	this.legalMC.parent = this;
	this.legalMC.setTransform(159.95,229.6,1,1,0,0,0,134.2,7.6);
	this.legalMC.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.legalMC},{t:this.l1MC},{t:this.l2MC}]}).wait(1));

	// f1
	this.t3MC = new lib.t3mc();
	this.t3MC.name = "t3MC";
	this.t3MC.parent = this;
	this.t3MC.setTransform(160.65,-242.4,1,1,0,0,0,86,35.6);

	this.t2MC = new lib.t2mc();
	this.t2MC.name = "t2MC";
	this.t2MC.parent = this;
	this.t2MC.setTransform(232.55,-284.4,1,1,0,0,0,27.9,35.6);

	this.t1MC = new lib.t1mc();
	this.t1MC.name = "t1MC";
	this.t1MC.parent = this;
	this.t1MC.setTransform(130.8,-284.4,1,1,0,0,0,70.2,35.6);

	this.toplineMC = new lib.toplinemc();
	this.toplineMC.name = "toplineMC";
	this.toplineMC.parent = this;
	this.toplineMC.setTransform(80,-32.5,1,1,0,0,0,20,7.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.toplineMC},{t:this.t1MC},{t:this.t2MC},{t:this.t3MC}]}).wait(1));

	// f2
	this.strip1MC = new lib.strip2MC();
	this.strip1MC.name = "strip1MC";
	this.strip1MC.parent = this;
	this.strip1MC.setTransform(144,-320);

	this.strip0MC = new lib.strip1MC();
	this.strip0MC.name = "strip0MC";
	this.strip0MC.parent = this;
	this.strip0MC.setTransform(-20,-274);

	this.redBG = new lib.redBG();
	this.redBG.name = "redBG";
	this.redBG.parent = this;
	this.redBG.setTransform(150,395,1,1,0,0,0,150,125);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.redBG},{t:this.strip0MC},{t:this.strip1MC}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(30,-334,419.1,1704);
// library properties:
lib.properties = {
	id: '8D1598B6579E4EADB310E734EDB2EB4E',
	width: 320,
	height: 240,
	fps: 60,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/box1.png", id:"box1"},
		{src:"images/box2.png", id:"box2"},
		{src:"images/box3.png", id:"box3"},
		{src:"images/box4.png", id:"box4"},
		{src:"images/box5.png", id:"box5"},
		{src:"images/logo.png", id:"logo"},
		{src:"images/mealbox.png", id:"mealbox"},
		{src:"images/puppet.png", id:"puppet"},
		{src:"images/strip1.png", id:"strip1"},
		{src:"images/strip2.png", id:"strip2"},
		{src:"images/strip3.png", id:"strip3"},
		{src:"images/strip4.png", id:"strip4"},
		{src:"images/strip5.png", id:"strip5"},
		{src:"images/strip6.png", id:"strip6"},
		{src:"images/strip7.png", id:"strip7"},
		{src:"images/strip8.png", id:"strip8"},
		{src:"images/strip9.png", id:"strip9"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['8D1598B6579E4EADB310E734EDB2EB4E'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;